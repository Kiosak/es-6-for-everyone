import View from './view';

class FighterInfo extends View {
   constructor(fighter, handlePickFighter) {
      super();
      this.showInfo(fighter, handlePickFighter);
      this.closeInfo = this.closeInfo.bind(this);
   }

   showInfo(fighter, handlePickFighter) {
      const { name, health, attack, defense } = fighter;
      const healthInfo = this.createInput('Health', health);
      const attackInfo = this.createInput('Attack', attack);
      const defenseInfo = this.createInput('Defense', defense);

      const closeBtn = this.createElement({
         tagName: 'div',
         className: 'close',
      });
      closeBtn.innerHTML = '&#x274C;';
      closeBtn.addEventListener('click', () => this.closeInfo(), false);

      const pickBtn = this.createElement({
         tagName: 'button',
         className: 'pick-btn',
      });
      pickBtn.innerHTML = 'Pick';
      pickBtn.addEventListener(
         'click',
         () => {
            handlePickFighter(
               this.pickFighter({
                  ...fighter,
                  health: healthInfo.lastChild.value,
                  attack: attackInfo.lastChild.value,
                  defense: defenseInfo.lastChild.value,
               })
            );
         },
         false
      );

      const infoContent = this.createElement({
         tagName: 'div',
         className: 'info__content',
      });
      infoContent.innerHTML = `<h3>${name}</h3>`;
      infoContent.append(
         healthInfo,
         attackInfo,
         defenseInfo,
         closeBtn,
         pickBtn
      );

      this.element = this.createElement({
         tagName: 'div',
         className: 'info',
      });
      this.element.append(infoContent);
   }

   createInput(labelName, val) {
      const label = this.createElement({
         tagName: 'label',
         className: 'label',
      });
      label.innerHTML = labelName;

      const input = this.createElement({
         tagName: 'input',
         className: 'details',
         attributes: { value: val },
      });
      label.appendChild(input);

      return label;
   }

   closeInfo() {
      if (this.element.parentNode)
         this.element.parentNode.removeChild(this.element);
   }

   pickFighter(fighter) {
      this.closeInfo();
      return fighter;
   }
}

export default FighterInfo;
