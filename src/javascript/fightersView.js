import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import FighterInfo from './fighterInfo';
import Fighter from './fighter';
import Fight from './fight';

class FightersView extends View {
   constructor(fighters) {
      super();

      this.handleClick = this.handleFighterClick.bind(this);
      this.handleInfoPickFighter = this.handleInfoPickFighter.bind(this);
      this.createFighters(fighters, this.handleFighterClick);
   }

   fightersDetailsMap = new Map();
   fightersMap = new Map();

   createFighters(fighters) {
      const fighterElements = fighters.map(fighter => {
         const fighterView = new FighterView(fighter, this.handleClick);
         return fighterView.element;
      });

      this.element = this.createElement({
         tagName: 'div',
         className: 'fighters',
      });
      this.element.append(...fighterElements);
   }

   async handleFighterClick(event, fighter) {
      const id = fighter._id;
      if (!this.fightersDetailsMap.has(id)) {
         const fighterDetails = await fighterService.getFighterDetails(id);
         this.fightersDetailsMap.set(id, { ...fighter, ...fighterDetails });
      }

      const details = this.fightersDetailsMap.get(id);
      const info = new FighterInfo(details, this.handleInfoPickFighter);

      this.element.appendChild(info.element);
   }

   handleInfoPickFighter(fighter, event) {
      const fighterName = new Fighter(fighter);
      this.fightersMap.set(fighterName.name, fighterName);
      if (this.fightersMap.size === 2) {
         this.prepareFight(this.fightersMap);
      }
   }

   prepareFight(fighters) {
      const [first, second] = Array.from(fighters.values());
      const fightersElements = [...document.querySelectorAll('.fighter')];
      fightersElements
         .filter(x => {
            return x.innerText != first.name && x.innerText != second.name
               ? x
               : null;
         })
         .forEach(x => x.classList.add('hide'));
      //new Fight(first, second);
   }
}

export default FightersView;
