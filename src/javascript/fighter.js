class Fighter {
   constructor({ _id, name, health, attack, defense }) {
      this._id = _id;
      this.name = name;
      this.health = health;
      this.attack = attack;
      this.defense = defense;
      this.getHitPower = this.getHitPower;
      this.getBlockPower = this.getBlockPower;
   }

   getChance() {
      return Math.floor(Math.random() * 2 + 1);
   }

   getHitPower() {
      return this.attack * this.getChance();
   }

   getBlockPower() {
      return this.defense * this.getChance();
   }
}

export default Fighter;
